from flask import Flask, render_template, request, redirect, url_for, jsonify, Response
import random, json
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
	r = json.dumps(books)
	return Response(r)

@app.route('/')
@app.route('/book/')
def showBook():
	return render_template('showBook.html', books = books)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	if request.method == 'POST':
		new_title = request.form['name']

		new_id = len(books)+1
		for i in range(len(books)):
			if new_id == int(books[i]['id']):
				new_id+=1
			else:
				break
		book = {'title': new_title, 'id': str(new_id)}
		books.append(book)
		return redirect(url_for('showBook', books = books))
	else:
		return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	if request.method == 'POST':
		edit_key = request.form['name']
		for i in range(len(books)):
			if book_id == int(books[i]['id']):
				books[i]['title'] = edit_key
		return redirect(url_for('showBook', books = books))
	else:
		return render_template('editBook.html', book_id = book_id)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	if request.method == 'POST':
		books[:] = [d for d in books if int(d['id']) != book_id]
		return redirect(url_for('showBook', books = books))
	else:
		return render_template('deleteBook.html', book_id = book_id)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
